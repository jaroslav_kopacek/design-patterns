#include "singlton.h"

static std::shared_ptr <Singlton> instance = nullptr;

Singlton::Singlton () {}

const std::shared_ptr <Singlton> Singlton::get_instance () {
    /* If instance is not created zyet then create it */
    if (!instance) {
        instance = std::shared_ptr <Singlton> (new Singlton);
    }

    return instance;
}
