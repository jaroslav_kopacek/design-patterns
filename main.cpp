#include <iostream>
#include "builder.h"
#include "factory.h"
#include "flyweight.h"
#include "observer.h"
#include "singlton.h"
#include "state.h"
#include "strategy.h"
#include "templated_method.h"

using namespace std;

int main() {
    const std::shared_ptr <Singlton> singlton = Singlton::get_instance();
    if (singlton) {
        cout << "Singleton created!" << endl;
    }


    vehicle_factory abstract_factory;
    std::shared_ptr <factory> cars = abstract_factory.get_factory(vehicle_factory::type::CAR);
    std::shared_ptr <vehicle> car_inst = cars->get_instance();
    car_inst->print();

    std::shared_ptr <factory> motorcycles = abstract_factory.get_factory(vehicle_factory::type::MOTO);
    std::shared_ptr <vehicle> moto_inst = motorcycles->get_instance();
    moto_inst->print();


    builder::car car_to_build;
    car_to_build.add_part(new builder::alloy_wheel ());
    car_to_build.add_part(new builder::performance_engine ());
    car_to_build.describe();


    std::shared_ptr <logged_user> user = std::make_shared <logged_user> ();
    std::shared_ptr <delivery_form> deliv_form = std::make_shared <delivery_form> (user);
    std::shared_ptr <billing_form> bill_form = std::make_shared <billing_form> (user);
    user->add_observer(deliv_form);
    user->add_observer(bill_form);

    user->set_name (std::string ("USER'S NAME"));
    user->set_mail(std::string ("USER@MAIL.CZ"));
    user->set_address(std::string ("SOMEWHERE 123, FAR AWAY"));

    user->remove_observer(deliv_form);
    user->remove_observer(bill_form);


    shared_state p1_internal_state ("PLAYER 1", shared_state::colour::RED);
    std::vector <soldier> p1_soldiers (10);
    shared_state p2_internal_state ("PLAYER 2", shared_state::colour::WHITE);
    std::vector <soldier> p2_soldiers (10);

    std::for_each (p1_soldiers.begin(), p1_soldiers.end(), [&] (soldier& __soldier) {__soldier.action(p1_internal_state);});
    std::for_each (p2_soldiers.begin(), p2_soldiers.end(), [&] (soldier& __soldier) {__soldier.action(p2_internal_state);});

    std::shared_ptr <shared_state> p_internal_state = std::make_shared <shared_state> ("ELF 1", shared_state::colour::BLUE);
    std::vector <elf> p_elfs (10, p_internal_state);
    std::for_each (p_elfs.begin(), p_elfs.end(), [&] (elf& __soldier) {__soldier.action();});

    tax ctx_full (new VAT);
    tax ctx_reduced (new reduced_VAT);

    ctx_full.compute (80);
    ctx_reduced.compute (100);

    cook chef;
    meat_meal meat;
    chef.make_meal (std::move (meat));
    veggie_meal veggie;
    chef.make_meal (std::move (veggie));

    connector connection;
    connection.connect ();
    connection.send (std::make_shared <boost::asio::streambuf> ());
    connection.receive (std::make_shared <boost::asio::streambuf> ());
    connection.disconnect ();
    connection.connect ();
    connection.send (std::make_shared <boost::asio::streambuf> ());
    connection.receive (std::make_shared <boost::asio::streambuf> ());
    connection.disconnect ();



    return 0;
}
