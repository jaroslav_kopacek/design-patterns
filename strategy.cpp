#include "strategy.h"
#include <iostream>

strategy::~strategy() {}

void VAT::operator() (float __price) {
    float tax = 0.20;
    std::cout << "Price is:" << __price << "\ttax is " << (__price * tax) << "\tprice inc. VAT  is " << (__price * (1 + tax)) << std::endl;
}

void reduced_VAT::operator() (float __price) {
    float tax = 0.15;
    std::cout << "Price is:" << __price << "\treduced tax is " << (__price * tax) << "\tprice inc. VAT  is " << (__price * (1 + tax)) << std::endl;
}

tax::tax (strategy* __strategy) : m_operation (__strategy) {}

void tax::compute (float __price) {
    (*m_operation) (__price);
}

tax::~tax () {
    delete m_operation;
}

