#include "flyweight.h"

shared_state::shared_state (std::string __player_name, colour __colour)
    : m_colour (__colour), m_player_name (__player_name) {}

shared_state::colour shared_state::get_colour() {
    return m_colour;
}

std::string shared_state::get_name() {
    return m_player_name;
}

void soldier::action(shared_state& __state) {
    auto player_colour = __state.get_colour();
    std::cout << __state.get_name() << " has " << (player_colour == shared_state::colour::RED ? "red" : player_colour == shared_state::colour::WHITE ?  "white" : "blue") << " colour." << std::endl;
}

elf::elf(std::shared_ptr<shared_state> __shared_state)
    : m_shared_state (__shared_state) {}

void elf::action () {
    auto player_colour = m_shared_state->get_colour();
    std::cout << m_shared_state->get_name() << " has " << (player_colour == shared_state::colour::RED ? "red" : player_colour == shared_state::colour::WHITE ?  "white" : "blue") << " colour." << std::endl;
}
