#ifndef FLYWEIGHT_H
#define FLYWEIGHT_H
#include <iostream>
#include <memory>
#include <string>

/**
 * @brief The shared_state class represents internal/shared state of group of same objects.
 */
class shared_state {
public:
    enum class colour {
        RED,
        WHITE,
        BLUE
    };

private:
    colour m_colour;
    std::string m_player_name;

public:
    shared_state (std::string __player_name, colour __colour);

    colour get_colour ();
    std::string get_name ();
};

/**
 * @brief The soldier class is class with manz instancies with shared part of state.
 */
class soldier {
public:

    /**
     * @brief Method action uses shared part of internal state.
     * @param __state [in] shared part of internal state
     */
    void action (shared_state& __state);
};

/**
 * @brief The elf class show how to solve same problem in different way.
 * @note This solution is not always taken as Flyweight pattern (different) implementation.
 */
class elf {
    /* Shared state */
    std::shared_ptr <shared_state> m_shared_state;

public:
    elf (std::shared_ptr <shared_state> __shared_state);

    /**
     * @brief Method action uses shared state stored as attribute not as param.
     */
    void action ();
};


#endif // FLYWEIGHT_H
