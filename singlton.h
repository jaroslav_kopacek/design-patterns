#ifndef SINGLTON_H
#define SINGLTON_H
#include <memory>

/**
 * @brief The Singlton class represents class where we want to exactly one instance exist.
 */
class Singlton {
private:
    Singlton ();
public:
    /**
     * @brief Method get_instance returns pointer to existing instance of this class. If instance
     * is not created before then it will be created.
     * @return Pointer to the instance.
     * @note Returned pointer is qualified as const to prevent reseting the shared pointer (shared
     * pointer's reset non-argument method will be inaccessible and rest of variants requires
     * public c-tor which isn't public in this case).
     */
    static const std::shared_ptr<Singlton> get_instance ();
};

#endif // SINGLTON_H
