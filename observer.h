#ifndef OBSERVER_H
#define OBSERVER_H
#include <memory>
#include <string>
#include <vector>

class observer;

/**
 * @brief The logged_user class represents observed class.
 */
class logged_user {
    std::string m_name;
    std::string m_address;
    std::string m_mail;
    /* Observers storage */
    std::vector <std::shared_ptr <observer>> m_observers;

public:
    /**
     * @brief Method add_observer adds observing object.
     * @param __observer [inout] observer object
     */
    void add_observer (std::shared_ptr <observer> __observer);

    /**
     * @brief Method remove_observer removes observing objsect.
     * @param __observer [in] observing object
     */
    void remove_observer (std::shared_ptr <observer> __observer);

    /**
     * @brief Method notify is called in setters to inform all observers about change.
     */
    void notify ();

    /**
     * @brief Method set_name is setter for name attribute.
     * @param __name [in] user's name
     */
    void set_name (std::string&& __name);

    /**
     * @brief Method set_address os setter for address attribute.
     * @param __address [in] user's address
     */
    void set_address (std::string&& __address);

    /**
     * @brief Method set_mail is setter for mail attribute.
     * @param __mail [in] user's mail
     */
    void set_mail (std::string&& __mail);

    /**
     * @brief Method get_name is name getter.
     * @return user's name
     */
    std::string get_name ();

    /**
     * @brief Method get_address is address getter.
     * @return user's address
     */
    std::string get_address ();

    /**
     * @brief Method get_mail is mail getter.
     * @return user's mail address
     */
    std::string get_mail ();

};

/**
 * @brief The observer class is interface for observing classes.
 */
class observer {
public:
    /**
     * @brief Method update actualize currently stored data with new one from observed object.
     */
    virtual void update () = 0;
};

class delivery_form : public observer {
    /**
     * @brief Instance of observed object used by @fn update to obtain new data.
     */
    std::shared_ptr <logged_user> m_user;
    std::string m_name = {"UNKNOWN"};
    std::string m_address = {"UNKNOWN"};
    std::string m_mail = {"UNKNOWN"};

public:
    delivery_form (std::shared_ptr <logged_user> __user);

    void update ();
};

class billing_form : public observer {
    /**
     * @brief Instance of observed object used by @fn update to obtain new data.
     */
    std::shared_ptr <logged_user> m_user;
    std::string m_name = {"UNKNOWN"};
    std::string m_address = {"UNKNOWN"};

public:
    billing_form(std::shared_ptr <logged_user> __user);

    void update ();

    /**
     * @brief Method set_name eexplicitly overwrites current name.
     * @param __name [inout] explicit name value
     */
    void set_name (std::string&& __name);

    /**
     * @brief Method set_address explicitly overwrites current address.
     * @param __address [inout] explicit address value
     */
    void set_address (std::string&& __address);
};


#endif // OBSERVER_H
