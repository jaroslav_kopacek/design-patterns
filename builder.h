#ifndef BUILDER_H
#define BUILDER_H
#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>
#include <algorithm>

namespace builder {
/************************************************
 *** ADDITIONAL ITEMS ACCEPTED BY MAIN CLASS ****
 ************************************************/
/**
 * @brief The part class represents interface for objects representing each type of car part.
 */
class part {
public:
    virtual std::string name () = 0;
    virtual ~part () {};
};

class wheel : public part {};

class steel_wheel : public wheel {
public:
    steel_wheel () {}
    std::string name ();;
};

class alloy_wheel : public wheel {
public:
    alloy_wheel () {}
    std::string name ();;
};

class performance_engine : public part  {
public:
    performance_engine () {}
    std::string name ();
};

class eco_engine : public part  {
public:
    eco_engine () {}
    std::string name ();;
};


/************************************************
 ******************* MAIN CLASS *****************
 ************************************************/
class car {
    /**
     * @brief Storage of added properties (in this case car parts).
     */
    std::vector <part*> m_parts;

public:
    car() {};
    ~car ();

    /**
     * @brief Method add_part adds properties to this object.
     * @param __part [in] object representing new feature.
     */
    void add_part (part* __part);

    /**
     * @brief Method add_parts_chain adds properties to this object.
     * @param __part [in] object representing new feature.
     * @note This function do same thing as @fn add_part but allows us to chaining
     * calls (i.e. instance.add_parts_chain (ftr1).add_parts_chain (ftr2);)
     */
    car& add_parts_chain (part* __part);

    /**
     * @brief Method describe print out currently added properties.
     */
    void describe ();

};

}

#endif // BUILDER_H
