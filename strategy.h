#ifndef STRATEGY_H
#define STRATEGY_H

class strategy {
public:
    virtual void operator() (float __price) = 0;

    virtual ~strategy ();
};

class VAT : public strategy {
public:
    void operator() (float __price);
};

class reduced_VAT : public strategy {
public:
    void operator() (float __price);
};

class tax {
    strategy* m_operation;
public:
    tax (strategy* __strategy);

    void compute (float __price);

    ~tax ();
};

#endif // STRATEGY_H
