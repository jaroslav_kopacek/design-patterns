#ifndef FACTORY_H
#define FACTORY_H
#include <memory>

/**
 * @brief The vehicle abstract class defines interface for factorized classes.
 */
class vehicle {
public:
    /**
     * @brief Method print just print out identifying string.
     */
    virtual void print () = 0;
    /**
     * @brief ~vehicle d-tor has to be virtual
     */
    virtual ~vehicle ();
};

/**
 * @brief The car class represents first of constructed objects.
 */
class car : public vehicle {
public:
    car () = default;
    virtual ~car ();;

    virtual void print ();
};

/**
 * @brief The moto class represents second of constructed objects.
 */
class moto : public vehicle {
public:
    moto () = default;
    virtual ~moto ();

    virtual void print ();
};

/**
 * @brief The factory class represents interface for factory classes.
 */
class factory {
public:
    virtual ~factory () {};
    virtual std::shared_ptr<vehicle> get_instance () = 0;
};

/**
 * @brief The car_factory class represents first of factory.
 */
class car_factory : public factory {
public:
    car_factory () = default;
    virtual ~car_factory ();;

    virtual std::shared_ptr<vehicle> get_instance ();
};

/**
 * @brief The moto_factory class represents second of factory.
 */
class moto_factory : public factory {
public:
    moto_factory () = default;
    virtual ~moto_factory ();;

    virtual std::shared_ptr<vehicle> get_instance ();
};

/**
 * @brief The vehicle_factory class represents abstract factory returning one of supported factories.
 */
class vehicle_factory {
public:
    /**
     * @brief The type enum is used to selecting instance of factory to return.
     */
    enum class type {
        CAR,
        MOTO
    };

    vehicle_factory ();

    /**
     * @brief Method get_factory returns proper instance of supported factory dependent on type passed by param.
     * @param __factory_type [in] type of factory
     * @returns Instance of selected factory.
     * @throws std::runtime+error when unimplemented type passed as param
     */
    std::shared_ptr<factory> get_factory (type __factory_type);
};



#endif // FACTORY_H
