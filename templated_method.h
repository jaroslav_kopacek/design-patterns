#ifndef TEMPLATED_METHOD_H
#define TEMPLATED_METHOD_H

/**
 * @brief The meal class represents algorithm composed from several steps and
 * derived class will implements these steps and therefore final behavior
 * will be different.
 */
class meal {
protected:
    virtual void step_1 () = 0;
    virtual void step_2 () = 0;
    virtual void step_3 () = 0;
public:
    /**
     * @brief Method make_mealrepresents algorithm to make meal which is composed
     * from several steps. Concrete classes implement each step of the algorith.
     */
    void make_meal ();
    virtual ~meal () {}
};


class veggie_meal : public meal {
protected:
    void step_1 ();
    void step_2 ();
    void step_3 ();
};

class meat_meal : public meal {
protected:
    void step_1 ();
    void step_2 ();
    void step_3 ();
};


/**
 * @brief The cook class represent kontext using defined algorith. In this case algorith to make a meal.
 */
class cook {
public:
    /**
     * @brief Method make_meal use obtained algorithm to make a meal;
     * @param __meal [inout] specific algorithm to make a meal.
     */
    void make_meal (meal&& __meal);
};

#endif // TEMPLATED_METHOD_H
