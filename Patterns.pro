TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        builder.cpp \
        factory.cpp \
        flyweight.cpp \
        main.cpp \
        observer.cpp \
        singlton.cpp \
        state.cpp \
        strategy.cpp \
        templated_method.cpp

HEADERS += \
    builder.h \
    factory.h \
    flyweight.h \
    observer.h \
    singlton.h \
    state.h \
    strategy.h \
    strategy.h \
    templated_method.h

QMAKE_CXXFLAGS += -std=c++17
