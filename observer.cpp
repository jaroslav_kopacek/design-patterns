#include "observer.h"
#include <algorithm>
#include <iostream>

void logged_user::add_observer (std::shared_ptr<observer> __observer) {
    m_observers.push_back (__observer);
}

void logged_user::remove_observer (std::shared_ptr<observer> __observer) {
    auto observer_it = std::find (m_observers.begin(), m_observers.end(), __observer);
    if (observer_it != m_observers.end()) {
        m_observers.erase (observer_it);
    }
    else {
        std::cerr << "No observer to remove - observer not found" << std::endl;
    }
}

void logged_user::notify () {
    std::for_each (m_observers.cbegin(), m_observers.cend(), [] (auto __observer) {__observer.get()->update();});
}

void logged_user::set_name (std::string&& __name) {
    m_name = std::move (__name);
    std::cout << "User's name set" << std::endl;
    notify();
}

void logged_user::set_address (std::string&& __address) {
    m_address = std::move (__address);
    std::cout << "User's address set" << std::endl;
    notify();
}

void logged_user::set_mail (std::string&& __mail) {
    m_mail = std::move (__mail);
    std::cout << "User's mail set" << std::endl;
    notify();
}

std::string logged_user::get_name () {
    return m_name;
}

std::string logged_user::get_address () {
    return m_address;
}

std::string logged_user::get_mail () {
    return m_mail;
}

delivery_form::delivery_form (std::shared_ptr<logged_user> __user) : m_user (__user) {}

void delivery_form::update () {
    m_name = m_user->get_name ();
    m_address = m_user->get_address ();
    m_mail = m_user->get_mail ();

    std::cout << "Current delivery data: ";
    if (m_name.size()) {
        std::cout << "name is: " <<  m_name;
    }
    if (m_address.size()) {
        std::cout << " address is: " << m_address;
    }
    if (m_mail.size()) {
        std::cout << " mail is:  " << m_mail;
    }
    std::cout << std::endl;
}

billing_form::billing_form (std::shared_ptr<logged_user> __user) : m_user (__user) {}

void billing_form::update() {
    m_name = m_user->get_name ();
    m_address = m_user->get_address ();

    std::cout << "Current billing data: ";
    if (m_name.size()) {
        std::cout << "name is: " <<  m_name;
    }
    if (m_address.size()) {
        std::cout << " address is: " << m_address;
    }

    std::cout << std::endl;
}

void billing_form::set_name (std::string&& __name) {
    m_name = std::move (__name);
}

void billing_form::set_address (std::string&& __address) {
    m_address = std::move (__address);
}
