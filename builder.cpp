#include "builder.h"

std::string builder::steel_wheel::name () {return "STEEL WHEEL";}

std::string builder::alloy_wheel::name () {return "ALLOY WHEEL";}

std::string builder::performance_engine::name () {return "V8 ENGINE";}

std::string builder::eco_engine::name () {return "L4 ENGINE";}

void builder::car::add_part (builder::part* __part) {
    if (!__part) {
        throw std::runtime_error ("No part instance to add");
    }

    m_parts.push_back (__part);
}

builder::car& builder::car::add_parts_chain (builder::part* __part) {
    add_part (__part);

    return *this;
}

void builder::car::describe () {
    std::cout << "CAR BUILD" << std::endl;
    for (const auto part : m_parts) {
        std::cout << "\tWITH " << part->name () << std::endl;
    }
}

builder::car::~car () {
    std::for_each (m_parts.begin (), m_parts.end (), [] (part* __part_it) {delete __part_it;});
}
