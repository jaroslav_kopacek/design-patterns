#ifndef STATE_H
#define STATE_H
#include <memory>
#include <any>
#include <string>
#include <variant>
#include <vector>
#include <boost/asio/streambuf.hpp>

class connector;

/**
 * @brief The state class represents interface for ops dependent on a state.
 */
class state {
public:
    virtual bool connect (connector& __connector) = 0;
    virtual bool disconnect (connector& __connector) = 0;
    virtual bool send (connector& __connector, std::shared_ptr <boost::asio::streambuf> __data) = 0;
    virtual bool receive (connector& __connector, std::shared_ptr <boost::asio::streambuf> __data) = 0;

    virtual ~state () {}
};

class connected : public state {
public:
    bool connect (connector& __connector);
    bool disconnect (connector& __connector);
    bool send (connector& __connector, std::shared_ptr <boost::asio::streambuf> __data);
    bool receive (connector& __connector, std::shared_ptr <boost::asio::streambuf> __data);
};

class disconnected : public state {
public:
    bool connect (connector& __connector);
    bool disconnect (connector& __connector);
    bool send (connector& __connector, std::shared_ptr <boost::asio::streambuf> __data);
    bool receive (connector& __connector, std::shared_ptr <boost::asio::streambuf> __data);
};


/**
 * @brief The connector class represents some connection class where offered
 * operations depend on current state of a connection.
 */
class connector {
    state* m_state = nullptr;

public:
    connector ();
    void set_state (state* __new_state);

    bool connect ();
    bool disconnect ();
    bool send (std::shared_ptr <boost::asio::streambuf> __data);
    bool receive (std::shared_ptr <boost::asio::streambuf> __data);

};


#endif // STATE_H
