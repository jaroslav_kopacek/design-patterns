#include "state.h"
#include <iostream>
#include <random>

/* Random generator is used for all methods due to simplzfication. Don't do this in real tasks! */
std::random_device rd;
std::minstd_rand0 gen(rd());
std::uniform_int_distribution<> distrib(1, 2);

bool connected::connect (connector& __connector [[maybe_unused]]) {
    std::cout << "CONNECTED: Connection already established" << std::endl;

    return false;
}

bool connected::disconnect (connector& __connector) {
    __connector.set_state(new disconnected ());
    std::cout << "CONNECTED: Sucessfully disconnected" << std::endl;

    return true;
}

bool connected::send (connector& __connector [[maybe_unused]], std::shared_ptr <boost::asio::streambuf> __data [[maybe_unused]]) {
    if (distrib (gen) / 2) {    // !send_data()
        std::cout << "CONNECTED: Send failed" << std::endl;

        return false;
    }

    std::cout << "CONNECTED: Data has been send" << std::endl;

    return true;
}

bool connected::receive (connector& __connector [[maybe_unused]], std::shared_ptr <boost::asio::streambuf> __data [[maybe_unused]]) {
    if (distrib (gen) / 2) {    // !send_data()
        std::cout << "CONNECTED: Read failed" << std::endl;

        return false;
    }

    // Write data into streambuf

    std::cout << "CONNECTED: Received new data" << std::endl;

    return true;

}

bool disconnected::connect (connector& __connector) {
    if (distrib (gen) / 2) {
        std::cout << "DISCONNECTED: Connected" << std::endl;
        __connector.set_state(new connected ());

        return true;
    }

    std::cout << "DISCONNECTED: Can't connect" << std::endl;

    return false;
}

bool disconnected::disconnect (connector& __connector [[maybe_unused]]) {
    std::cout << "DISCONNECTED: Connection not established yet" << std::endl;

    return false;
}

bool disconnected::send (connector& __connector [[maybe_unused]], std::shared_ptr <boost::asio::streambuf> __data [[maybe_unused]]) {
    std::cout << "DISCONNECTED: Connection not established yet" << std::endl;

    return false;
}

bool disconnected::receive (connector& __connector [[maybe_unused]], std::shared_ptr <boost::asio::streambuf> __data [[maybe_unused]]) {
    std::cout << "DISCONNECTED: Connection not established yet" << std::endl;

    return false;
}

connector::connector() : m_state (new disconnected) {}

void connector::set_state(state* __new_state) {
    std::swap(m_state, __new_state);

    delete __new_state;

}

bool connector::connect() {
    try {
        return m_state->connect(*this);
    }
    catch  (...) {
        // LOG ERROR
    }

    return false;
}

bool connector::disconnect() {
    try {
        return m_state->disconnect(*this);
    }
    catch  (...) {
        // LOG ERROR
    }

    return false;
}

bool connector::send(std::shared_ptr<boost::asio::streambuf> __data) {
    try {
        return m_state->send (*this, __data);
    }
    catch  (...) {
        // LOG ERROR
    }

    return false;

}

bool connector::receive(std::shared_ptr<boost::asio::streambuf> __data) {
    try {
        return m_state->receive (*this, __data);
    }
    catch  (...) {
        // LOG ERROR
    }

    return false;
}
