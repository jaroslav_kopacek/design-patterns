#include "templated_method.h"
#include <iostream>

void meal::make_meal () {
    step_1 ();
    step_2 ();
    step_3 ();
}

void veggie_meal::step_1 () {
    std::cout << "Lettuce with ";
}

void veggie_meal::step_2 () {
    std::cout << "baked potatoes and ";
}

void veggie_meal::step_3 () {
    std::cout << "garlic dip." << std::endl;
}

void meat_meal::step_1 () {
    std::cout << "Beef steak with ";
}

void meat_meal::step_2 () {
    std::cout << "franch fries and ";
}

void meat_meal::step_3 () {
    std::cout << "peppercorn dip." << std::endl;
}

void cook::make_meal(meal&& __meal) {
    __meal.make_meal();
}
