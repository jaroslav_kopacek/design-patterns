#include "factory.h"
#include <stdexcept>
#include <iostream>
#include <memory>

std::shared_ptr<vehicle> car_factory::get_instance () {
    return std::make_shared <car> ();
}

car_factory::~car_factory () {}

std::shared_ptr <vehicle> moto_factory::get_instance () {
    return std::make_shared <moto> ();
}

moto_factory::~moto_factory () {}

vehicle_factory::vehicle_factory () {}

std::shared_ptr <factory> vehicle_factory::get_factory (vehicle_factory::type __factory_type) {
    switch (__factory_type) {
        case vehicle_factory::type::CAR:
            return std::make_shared <car_factory> ();
        case vehicle_factory::type::MOTO:
            return std::make_shared <moto_factory> ();
        default:
            throw std::runtime_error ("Unimplemented factory");
    }
}

vehicle::~vehicle () {}

car::~car () {}

void car::print () {
    std::cout << "CAR INSTANCE" << std::endl;
}

moto::~moto () {}

void moto::print () {
    std::cout << "MOTO INSTANCE" << std::endl;
}

